package com.rjgc.emallsimulation;

import com.rjgc.emallsimulation.entity.*;
import com.rjgc.emallsimulation.mapper.OrdersMapper;
import com.rjgc.emallsimulation.mapper.UserMapper;
import com.rjgc.emallsimulation.service.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class EmallsimulationApplicationTests {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserService userService;
    @Resource
    private OrdersService orderService;
    @Resource
    private StoreService storeService;
    @Resource
    private ItemService itemService;
    @Resource
    private OrderDetailService orderDetailService;
    @Resource
    private UserInfoService userInfoService;

    @Test
    public void selectAll(){
        List<User> users=userMapper.selectList(null);
        users.forEach(System.out::println);
        var user=userMapper.selectById(233231);
        System.out.println(user);
    }

    @Test
    public void selectAlls(){
        List<User> users=userService.list();
        users.forEach(System.out::println);
    }

    @Test
    public void order(){
        Orders order=new Orders();
        order.setDate(LocalDateTime.now());
        order.setState("未发货");
        order.setDeliveraddress("海口");
        order.setReceiveaddress("武汉");
        order.setDelivername("苏宣");
        order.setReceivename("武汉大学");
        order.setDeliverphone("173xxxxxxxx");
        order.setReceivephone("56789456");
        orderService.save(order);
    }

    @Test
    public void store(){
        List<Store> stores=storeService.list();
        stores.forEach(System.out::println);
    }

    @Test
    public void item(){
        List<Item> items=itemService.list();
        items.forEach(System.out::println);
    }

    @Test
    public void orderDetail(){
        List<OrderDetail> details=orderDetailService.list();
        details.forEach(System.out::println);
    }

    @Test
    public void userInfo(){
        List<UserInfo> infos=userInfoService.list();
        infos.forEach(System.out::println);
    }
}
