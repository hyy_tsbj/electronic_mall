package com.rjgc.emallsimulation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rjgc.emallsimulation.entity.Picture;

public interface PictureMapper extends BaseMapper<Picture> {
}
