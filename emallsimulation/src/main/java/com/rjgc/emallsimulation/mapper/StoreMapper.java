package com.rjgc.emallsimulation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rjgc.emallsimulation.entity.Store;

public interface StoreMapper extends BaseMapper<Store> {
}
