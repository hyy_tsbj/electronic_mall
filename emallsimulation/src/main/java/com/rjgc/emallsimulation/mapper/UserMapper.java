package com.rjgc.emallsimulation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rjgc.emallsimulation.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
