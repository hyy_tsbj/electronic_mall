package com.rjgc.emallsimulation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rjgc.emallsimulation.entity.UserInfo;

public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
