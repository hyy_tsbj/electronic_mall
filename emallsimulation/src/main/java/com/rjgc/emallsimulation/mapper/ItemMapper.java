package com.rjgc.emallsimulation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rjgc.emallsimulation.entity.Item;

public interface ItemMapper extends BaseMapper<Item> {
}
