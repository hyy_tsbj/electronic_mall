package com.rjgc.emallsimulation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.rjgc.emallsimulation.mapper")
public class EmallsimulationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmallsimulationApplication.class, args);
    }

}
