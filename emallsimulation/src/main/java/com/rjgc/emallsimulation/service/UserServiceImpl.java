package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rjgc.emallsimulation.entity.User;
import com.rjgc.emallsimulation.mapper.UserMapper;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
