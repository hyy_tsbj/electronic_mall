package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rjgc.emallsimulation.entity.UserInfo;

public interface UserInfoService extends IService<UserInfo> {
}
