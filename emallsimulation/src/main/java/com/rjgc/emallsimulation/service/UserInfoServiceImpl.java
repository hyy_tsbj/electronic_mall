package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rjgc.emallsimulation.entity.UserInfo;
import com.rjgc.emallsimulation.mapper.UserInfoMapper;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {
}
