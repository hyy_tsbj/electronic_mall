package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rjgc.emallsimulation.entity.Orders;

public interface OrdersService extends IService<Orders> {
}
