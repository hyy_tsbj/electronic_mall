package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rjgc.emallsimulation.entity.Picture;
import com.rjgc.emallsimulation.mapper.PictureMapper;
import org.springframework.stereotype.Service;

@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements PictureService{
}
