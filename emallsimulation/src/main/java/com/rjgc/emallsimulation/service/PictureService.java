package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rjgc.emallsimulation.entity.Picture;

public interface PictureService extends IService<Picture> {
}
