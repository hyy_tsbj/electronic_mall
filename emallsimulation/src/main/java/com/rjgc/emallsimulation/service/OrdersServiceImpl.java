package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rjgc.emallsimulation.entity.Orders;
import com.rjgc.emallsimulation.mapper.OrdersMapper;
import org.springframework.stereotype.Service;

@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {
}
