package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rjgc.emallsimulation.entity.User;

public interface UserService extends IService<User> {
}
