package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rjgc.emallsimulation.entity.OrderDetail;
import com.rjgc.emallsimulation.mapper.OrderDetailMapper;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
}
