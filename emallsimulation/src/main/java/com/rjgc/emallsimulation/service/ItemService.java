package com.rjgc.emallsimulation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rjgc.emallsimulation.entity.Item;

public interface ItemService extends IService<Item> {
}
