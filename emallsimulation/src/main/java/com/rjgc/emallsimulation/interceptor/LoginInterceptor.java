package com.rjgc.emallsimulation.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("=======登录拦截=======");
        HttpSession session=request.getSession();
        log.info("sessionId为："+session.getId());
        Object userinfo=session.getAttribute("userinfo");
        if(userinfo==null){
            log.info("用户未登录");
            response.setStatus(403);
            response.getWriter().println("please login");
            //response.sendRedirect("http://101.201.249.192/login");
            return false;
        }else {
            log.info("用户已登录");
        }

        return true;
    }
}
