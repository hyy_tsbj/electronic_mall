package com.rjgc.emallsimulation.adapter;

import com.rjgc.emallsimulation.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderAdapter {
    private int orderID;
    private String customerUserName;
    private LocalDate orderDate;
    private String orderState;
    private String orderRemarks;

    public static OrderAdapter toOrderAdp(Orders order){
        return new OrderAdapter(order.getOrderid(),order.getReceivename(),
                order.getDate().toLocalDate(),order.getState(),order.getRemark());
    }

    public static List<OrderAdapter> toOrderadpList(List<Orders> orders){
        List<OrderAdapter> adps=new ArrayList<>();
        for (var o:orders){
            adps.add(OrderAdapter.toOrderAdp(o));
        }
        return adps;
    }

}
