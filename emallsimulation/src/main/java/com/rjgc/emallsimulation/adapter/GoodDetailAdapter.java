package com.rjgc.emallsimulation.adapter;

import com.rjgc.emallsimulation.entity.OrderDetail;
import com.rjgc.emallsimulation.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodDetailAdapter {
    int goodsID;
    String goodsName;
    int goodsQuantity;
    BigDecimal goodsUnitPrice;
    BigDecimal itemPrice;

    public static GoodDetailAdapter toDetails(OrderDetail detail){
        BigDecimal total=detail.getItemprice().multiply(new BigDecimal(detail.getItemquantity()));
        return new GoodDetailAdapter(detail.getItemid(),detail.getItemname(),detail.getItemquantity(),detail.getItemprice(),total);
    }

    public static List<GoodDetailAdapter> toDetailList(List<OrderDetail> orders){
        List<GoodDetailAdapter> adps=new ArrayList<>();
        for (var o:orders){
            adps.add(GoodDetailAdapter.toDetails(o));
        }
        return adps;
    }
}
