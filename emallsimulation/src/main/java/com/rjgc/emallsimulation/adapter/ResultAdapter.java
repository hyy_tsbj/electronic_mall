package com.rjgc.emallsimulation.adapter;

import java.util.HashMap;
import java.util.Map;

public class ResultAdapter {

    public static Map<String,Object> singleResult(String field,Object value){
        Map<String,Object> result=new HashMap<String,Object>();
        result.put(field,value);
        return result;
    }
}
