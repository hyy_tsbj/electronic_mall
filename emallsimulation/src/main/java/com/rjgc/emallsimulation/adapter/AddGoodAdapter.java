package com.rjgc.emallsimulation.adapter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddGoodAdapter {
    String phone;
    GoodInfoAdapter goodInfoAdapter;
}
