package com.rjgc.emallsimulation.adapter;

import com.rjgc.emallsimulation.entity.Item;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodInfoAdapter {
    int goodsID;
    String name;
    BigDecimal price;
    int quantity;
    String description;
    List<String> imgFileList;

    public static GoodInfoAdapter toGoodinfo(Item item,List<String> list){
        return new GoodInfoAdapter(item.getItemid(),item.getItemname(),
                item.getItemprice(),item.getInventory(),item.getDescription(),list);
    }

    public static Item toItem(GoodInfoAdapter good,int storeid){
        return new Item(good.getGoodsID(),good.getName(),
                good.getPrice(),good.getQuantity(),storeid,good.getDescription());
    }
}
