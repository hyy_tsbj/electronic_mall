package com.rjgc.emallsimulation.adapter;

import com.rjgc.emallsimulation.entity.Item;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodAdapter {
    int goodsID;
    String goodsName;
    int goodsQuantity;

    public static GoodAdapter toGood(Item item){
        return new GoodAdapter(item.getItemid(),item.getItemname(),item.getInventory());
    }

    public static List<GoodAdapter> toGoodList(List<Item> list){
        List<GoodAdapter> goods=new ArrayList<>();
        for (Item i:list){
            goods.add(GoodAdapter.toGood(i));
        }
        return goods;
    }

}
