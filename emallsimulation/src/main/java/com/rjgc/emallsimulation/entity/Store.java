package com.rjgc.emallsimulation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Store {
    int storeid;
    String store;
    String account;
    String sort;
}
