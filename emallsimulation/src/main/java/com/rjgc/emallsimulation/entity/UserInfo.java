package com.rjgc.emallsimulation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("userinf")
public class UserInfo {
    @TableId(value = "phone",type = IdType.ASSIGN_ID)
    String phone;
    String address;
    BigDecimal balance;
}
