package com.rjgc.emallsimulation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @TableId(value = "itemid",type = IdType.INPUT)
    Integer itemid;
    String itemname;
    BigDecimal itemprice;
    int inventory;//库存
    int storeid;
    String description;
}
