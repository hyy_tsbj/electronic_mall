package com.rjgc.emallsimulation.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("orderdetail")
public class OrderDetail {
    int orderid;
    int itemid;
    String itemname;
    BigDecimal itemprice;
    int itemquantity;
}
