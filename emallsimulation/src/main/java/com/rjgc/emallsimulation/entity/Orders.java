package com.rjgc.emallsimulation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("myorder")
public class Orders {
    @TableId(value = "orderid",type = IdType.INPUT)
    int orderid;
    LocalDateTime date;
    String state;
    String deliveraddress;
    String receiveaddress;
    String delivername;
    String receivename;
    String deliverphone;
    String receivephone;
    BigDecimal totalprice;
    String remark;
}
