package com.rjgc.emallsimulation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
//废弃
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goods {
    int goodId;
    String goodName;
    BigDecimal goodPrice;
    int inventory;//库存
    String description;
}
