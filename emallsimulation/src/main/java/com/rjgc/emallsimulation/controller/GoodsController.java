package com.rjgc.emallsimulation.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rjgc.emallsimulation.adapter.AddGoodAdapter;
import com.rjgc.emallsimulation.adapter.GoodAdapter;
import com.rjgc.emallsimulation.adapter.GoodInfoAdapter;
import com.rjgc.emallsimulation.adapter.ResultAdapter;
import com.rjgc.emallsimulation.entity.Item;
import com.rjgc.emallsimulation.entity.Picture;
import com.rjgc.emallsimulation.service.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class GoodsController {
    @Resource
    ItemService itemService;
    @Resource
    StoreService storeService;
    @Resource
    UserService userService;
    @Resource
    PictureService pictureService;

    //id=9
    @PostMapping("/addGood")
    @Transactional
    public Map<String,Object> addGood(
            @RequestParam("phone") String phone,
            @RequestParam("goodsID") int goodsid,
            @RequestParam("name") String name,
            @RequestParam("price") BigDecimal price,
            @RequestParam("quantity") int quantity,
            @RequestParam("description") String des,
            @RequestParam("imgFileList") List<String> imgs
    ){
        var store=getIdFromPhone(phone);
        Item i=new Item(goodsid,name,price,quantity,store,des);
        var flag=itemService.save(i);
        for (var p:imgs){
            pictureService.save(new Picture(goodsid,p));
        }
        if(flag){
            return ResultAdapter.singleResult("AddGoodsSuccess",true);
        }else {
            return ResultAdapter.singleResult("AddGoodsSuccess",false);
        }
    }

    @DeleteMapping("/deleteGood")
    public Map<String,Object> deleteGood(
            @RequestParam("phone") String phone,
            @RequestParam("goodsID") int id
    ){
        var store=getIdFromPhone(phone);
        var flag=itemService.remove(new QueryWrapper<Item>()
                .eq("itemid",id)
                .eq("storeid",store));
        if(flag){
            return ResultAdapter.singleResult("DeleteSuccess",true);
        }else {
            return ResultAdapter.singleResult("DeleteSuccess",false);
        }
    }

    @PutMapping("/updateGood")
    public Map<String,Object> updateGood(
            @RequestParam("phone") String phone,
            @RequestParam("goodsID") int good,
            @RequestParam("newGoodsID") int newid,
            @RequestParam("name") String name,
            @RequestParam("price") BigDecimal price,
            @RequestParam("quantity") int quantity,
            @RequestParam("description") String des,
            @RequestParam("imgFileList") List<String> imgs
    ){
        var store=getIdFromPhone(phone);
        var i=itemService.query().eq("itemid",good).eq("storeid",store).one();
        i.setItemid(newid);
        i.setItemname(name);
        i.setItemprice(price);
        i.setInventory(quantity);
        i.setDescription(des);
        pictureService.remove(new QueryWrapper<Picture>().eq("itemid",good));
        for (var p:imgs){
            pictureService.save(new Picture(newid,p));
        }
        var flag=itemService.saveOrUpdate(i);
        if(flag){
            return ResultAdapter.singleResult("editSuccess",true);
        }else {
            return ResultAdapter.singleResult("editSuccess",false);
        }
    }

    @GetMapping("/goodList/{phone}")
    public List<GoodAdapter> goodlist(
            @PathVariable("phone") String phone
    ){
        var store=getIdFromPhone(phone);
        return GoodAdapter.toGoodList(
                itemService.list(new QueryWrapper<Item>().eq("storeid",store))
        );
    }

    @GetMapping("/mgoodList/{phone}/{id}")
    public List<GoodAdapter> mgoodlist(
            @PathVariable("phone") String phone,
            @PathVariable("id") int id
    ){
        var store=getIdFromPhone(phone);
        return GoodAdapter.toGoodList(
                itemService.list(new QueryWrapper<Item>()
                        .eq("storeid",store)
                        .likeRight("itemid",id))
        );
    }

    @GetMapping("/good/{id}")
    public GoodInfoAdapter getGood(
         @PathVariable("id") int id
    ){
        var pictures=pictureService.query().eq("itemid",id).list();
        var imgs=getImgs(pictures);
        return GoodInfoAdapter.toGoodinfo(itemService.getById(id),imgs);
    }

    private int getIdFromPhone(String phone){
        var user=userService.query().eq("phone",phone).one();
        var store=storeService.query().eq("account",user.getAccount()).one();
        int id=store.getStoreid();
        return id;
    }

    private List<String> getImgs(List<Picture> pic){
        List<String> imgs=new ArrayList<>();
        for (var p:pic){
            imgs.add(p.getPicture());
        }
        return imgs;
    }
}
