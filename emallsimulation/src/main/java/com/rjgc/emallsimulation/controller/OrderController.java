package com.rjgc.emallsimulation.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.rjgc.emallsimulation.adapter.GoodDetailAdapter;
import com.rjgc.emallsimulation.adapter.OrderAdapter;
import com.rjgc.emallsimulation.adapter.ResultAdapter;
import com.rjgc.emallsimulation.entity.OrderDetail;
import com.rjgc.emallsimulation.entity.Orders;
import com.rjgc.emallsimulation.entity.User;
import com.rjgc.emallsimulation.service.OrderDetailService;
import com.rjgc.emallsimulation.service.OrdersService;
import com.rjgc.emallsimulation.service.StoreService;
import com.rjgc.emallsimulation.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {
    @Resource
    OrdersService ordersService;
    @Resource
    StoreService storeService;
    @Resource
    UserService userService;
    @Resource
    OrderDetailService orderDetailService;
    //id=6
    @PutMapping("/deliverOrder")
    public Map<String,Object> deliver(
            @RequestParam("phone") String phone,
            @RequestParam("orderID") int order
    ){
        int store=getIdFromPhone(phone);
        var o=ordersService.getById(order);
        o.setState("已发货");
        var flag=ordersService.updateById(o);
        if(flag){
            return ResultAdapter.singleResult("DeliverSuccess",true);
        }else {
            return ResultAdapter.singleResult("DeliverSuccess",true);
        }
    }
    //id=8
    @GetMapping("/orders/{phone}")
    public List<OrderAdapter> getOrders(
            @PathVariable("phone") String phone
    ){
        var orders=ordersService.query().eq("deliverphone",phone).list();
        return OrderAdapter.toOrderadpList(orders);
    }
    @GetMapping("/morders/{phone}/{id}")
    public List<OrderAdapter> mgetOrders(
            @PathVariable("phone") String phone,
            @PathVariable("id") int id
    ){
        var orders=ordersService.query().eq("deliverphone",phone).likeRight("orderid",id).list();
        return OrderAdapter.toOrderadpList(orders);
    }
    //id=12
    @GetMapping("/orderDetail/{phone}/{id}")
    public List<GoodDetailAdapter>  getDetails(
            @PathVariable("phone") String phone,
            @PathVariable("id") int id
    ){
        int store=getIdFromPhone(phone);
        return GoodDetailAdapter.toDetailList(orderDetailService.query().eq("orderid",id).list());
    }

    @GetMapping("/uorders/{phone}")
    public List<Map<String,Object>> uorders(
            @PathVariable("phone") String phone
    ){
        var orders=ordersService.query().eq("receivephone",phone).list();
        List<Map<String,Object>> res=new ArrayList<>();
        for(var o:orders){
            Map<String,Object> map=new HashMap<>();
            map.put("orderDate",o.getDate());
            map.put("orderID",o.getOrderid());
            map.put("orderState",o.getState());
            map.put("orderRemarks",o.getRemark());
            map.put("sellerPhone",o.getDeliverphone());
            map.put("totalPrice",o.getTotalprice());
            res.add(map);
        }
        return res;
    }

    private int getIdFromPhone(String phone){
        var user=userService.query().eq("phone",phone).one();
        var store=storeService.query().eq("account",user.getAccount()).one();
        int id=store.getStoreid();
        return id;
    }
}
